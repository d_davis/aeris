function initPlayer() {
	if(!document.getElementById('player')) {
		if(audioSourcesList.length != 0) {
			var player = document.createElement('div');
			player.id = "player";
			player.className = "container-fluid col-xs-12 col-sm-12 col-md-12 col-lg-12";

			var row = document.createElement('div');
			row.className = "row";

			var playerControls = document.createElement('div');
			playerControls.className = "playerControls col-xs-1 col-sm-1 col-md-1 col-lg-1";

			var buttonBackward = document.createElement('div');
			buttonBackward.className = "button-space";
			buttonBackward.innerHTML = "<button class='glyphicon glyphicon-step-backward' title='Previous' onclick='previousTrack()'></button>";

			var buttonForward = document.createElement('div');
			buttonForward.className = "button-space";
			buttonForward.innerHTML = "<button class='glyphicon glyphicon-step-forward' title='Next' onclick='nextTrack()'></button>";

			var buttonPlayPause = document.createElement('div');
			buttonPlayPause.className = "button-space";

			var buttonPlay = document.createElement('button');
			buttonPlay.className = "glyphicon glyphicon-play";
			buttonPlay.id = "audioPlay";
			buttonPlay.title = "Play";
			buttonPlay.onclick = function() { playPause() };

			var buttonPause = document.createElement('button');
			buttonPause.className = "hidden";
			buttonPause.id = "audioPause";
			buttonPause.title = "Pause";
			buttonPause.onclick = function() { playPause() };

			var audioElapsed = document.createElement('div');
			audioElapsed.className = "col-xs-1 col-sm-1 col-md-1 col-lg-1";
			audioElapsed.id = "audioElapsed";

			var playerSeekRow = document.createElement('div');
			//playerSeekRow.className = "col-xs-8 col-sm-8 col-md-8 col-lg-8";
			//playerSeekRow.style.top = "35px";

			var playerSeek =  document.createElement('div');
			playerSeek.className = "playerSeek";
			playerSeek.id = "playerSeek";

			var audioSeek = document.createElement('div');
			audioSeek.id = "audioSeek";

			var audioLoaded = document.createElement('div');
			audioLoaded.id = "audioLoaded";

			playerControls.appendChild(buttonBackward);
			buttonPlayPause.appendChild(buttonPlay);
			buttonPlayPause.appendChild(buttonPause);
			playerControls.appendChild(buttonPlayPause);
			playerControls.appendChild(buttonForward);
			playerSeek.appendChild(audioSeek);
			playerSeekRow.appendChild(playerSeek);
			audioSeek.appendChild(audioLoaded);
			row.appendChild(playerControls);
			row.appendChild(audioElapsed);
			row.appendChild(playerSeekRow);
			player.appendChild(row);
			document.getElementById('footer').appendChild(player);	

			var playerSeek = document.getElementById("playerSeek").offsetWidth;
			var audioSeek = document.getElementById("audioSeek");

			audioSeek.style.width = String(playerSeek);

			var clicking = false;

			$('#playerSeek').mousedown(function () {
				clicking = true;
			});

			$(document).mouseup(function () {
				clicking = false;
			});

			$('#playerSeek').mousemove(function(e) {
				if(clicking) { 
			        var posX = $(this).position().left;
			        var posY = $(this).position().top;
			        console.log("mousemove", posX, posY, e.pageX, e.pageY, (e.pageX - posX), e.clientX);
			        audioSeekWidth((parseFloat(e.pageX) - posX));
				}
			});
		}
	}
}

var audio = document.createElement('audio');
audio.addEventListener("timeupdate", setElapsed);

function playTrack(url) {

	var track;
	var player = false;

	for(var i = 0; i < audioSourcesList.length; i++){
		if(audioSourcesList[i].indexOf(url) != -1){
			track = i;
		}
	}

	if(document.getElementById('player')) {
		player = true;
	} else {
		initPlayer();
		player = true;
	}

	if(track != null && player) {
		audio.src = audioSourcesList[track][3];
		audio.id = url;
		//player.appendChild(audio);
	}
}

function audioSeekWidth(position) {
	var playerSeek = document.getElementById('playerSeek');
	audio.currentTime = audio.duration / playerSeek.offsetWidth * position;
	console.log("audioSeekWidth", audio.duration, playerSeek.offsetWidth, position);
}

function setDuration(event) {
	audioDuration.innerHTML = timeFormatter(audio.duration);
}

function setElapsed(event) {
	var audioElapsed = document.getElementById("audioElapsed");
	var audioLoaded = document.getElementById("audioLoaded");
	var playerSeek = document.getElementById('playerSeek');
	audioElapsed.innerHTML = "-" + timeFormatter(audio.duration - audio.currentTime);
	amountLoaded = (audio.currentTime/audio.duration)*playerSeek.offsetWidth;
	audioLoaded.style.width = amountLoaded + 'px';
}

function playPause() {
	var nowPlaying = document.getElementById(audio.id);
	var audioPlay = document.getElementById("audioPlay");
	var audioPause = document.getElementById("audioPause");

	if (audio.paused){
	audio.play();
	audioPlay.className = 'hidden';
	audioPause.className = 'glyphicon glyphicon-pause track';
	nowPlaying.className = 'glyphicon glyphicon-pause track';

	} else if (!audio.paused){
	audio.pause();
	audioPlay.className = 'glyphicon glyphicon-play track';
	audioPause.className = 'hidden';
	nowPlaying.className = 'glyphicon glyphicon-play track';
	}
}

/*function playStop() {
	audio.pause();
	audio.currentTime=0;
	audioPlay.className = 'glyphicon glyphicon-play';
	audioPause.className = 'hidden';
}*/

function nextTrack() {
	for(var i = 0; i < audioSourcesList.length; i++){
		if(audioSourcesList[i].indexOf(audio.src) != -1){
			var currentTrack = i;
		}
	}

	document.addEventListener("play", function(e){ 
		var tracksOnPage = document.getElementsByClassName('track');

	    for (var i = 0; i < tracksOnPage.length; i++) {
	      if(tracksOnPage[i] != e.target) {
	        tracksOnPage[i].className = 'glyphicon glyphicon-play track';
	      	}
	    }
	});

	var c = currentTrack + 1;
	console.log(currentTrack, c);
	if(audioSourcesList.length > c > -1) {
		audio.pause();
		audio.id = audioSourcesList[c][3];
		audio.src = audioSourcesList[c][3];
		playPause();
	} else if(sources.length <= c || i <= -1) {
		audio.id = audioSourcesList[0][3];
		audio.src = audioSourcesList[0][3];
		playPause();
	}
}

function previousTrack() {
	var sources = audioSourcesList;
	var currentTrack = sources.indexOf(document.getElementById('nowplaying').src);
	var i = currentTrack - 1;
	console.log(currentTrack, i);
	if(sources.length > i > -1) {
		audio.pause();
		audio.src = sources[i];
		audio.play();
	}
}

function timeFormatter(seconds){
	function zeroPad(str) {
		if (str.length > 2) return str;
		for (i=0; i<(2-str.length); i++) {
			str = "0" + str;
		}
		return str;
	}
	var minute = 60,
	hour = minute * 60,
	hStr = "",
	mStr = "",
	sStr = "";

	var h = Math.floor(seconds / hour);
	hStr = zeroPad(String(h));

	var m = Math.floor((seconds - (h * hour)) / minute);
	mStr = zeroPad(String(m));

	var s = Math.floor((seconds - (h * hour)) - (m * minute));
	sStr = zeroPad(String(s));
	return (hStr + ":" + mStr + ":" + sStr);
}
