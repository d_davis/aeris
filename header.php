<!doctype html>
<html>
<head>
  <meta charset='utf-8'>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Aeris</title>
  <?php

  $get_site_url = get_site_url();
  $get_theme_root_uri = get_theme_root_uri();
  $get_bloginfo_description = get_bloginfo('description');


  $wordpress_variables = array(
    'site_url' => $get_site_url,
    'theme_root_uri' => $get_theme_root_uri,
    'bloginfo_description' => $get_bloginfo_description
  );

  wp_register_script('contents', get_theme_root_uri() . '/aeris/contents.js');
  wp_localize_script('contents', 'wordpress_variables', $wordpress_variables);
  wp_enqueue_script('contents');
  wp_enqueue_style('bootstrap', get_theme_root_uri() . '/aeris/bootstrap.min.css' );
  wp_enqueue_script('bootstrap', get_theme_root_uri() . '/aeris/bootstrap.min.js');
  wp_enqueue_script('p5', get_theme_root_uri() . '/aeris/p5.min.js');
  wp_enqueue_script('p5dom', get_theme_root_uri() . '/aeris/p5.dom.min.js');
  wp_enqueue_script('p5sound', get_theme_root_uri() . '/aeris/p5.sound.min.js');
  //wp_enqueue_script('processing', get_theme_root_uri() . '/aeris/processing.min.js');
  //wp_enqueue_script('brownian', get_theme_root_uri() . '/aeris/brownian.js');
  wp_enqueue_script('triangulation', get_theme_root_uri() . '/aeris/triangles.js');
  wp_enqueue_style('evolventa', get_theme_root_uri() . '/aeris/evolventa/evolventa.css');
  wp_enqueue_style('perfect-scrollbar', get_theme_root_uri() . '/aeris/perfect-scrollbar.css');
  wp_enqueue_script('perfect-scrollbar', get_theme_root_uri() . '/aeris/perfect-scrollbar.min.js');
  wp_enqueue_style('style', get_theme_root_uri() . '/aeris/style.css');
  wp_enqueue_script('player', get_theme_root_uri() . '/aeris/player.js');
  wp_enqueue_style('player', get_theme_root_uri() . '/aeris/player.css');
  //wp_enqueue_script('howler', 'https://cdnjs.cloudflare.com/ajax/libs/howler/2.0.4/howler.js');

  wp_head();
  ?>

</head>
<?php flush();?>

<body>
<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" />
<div id='brownian'></div>
<!--<div id="player">inner content</div>-->

<nav class="navbar navbar-default navbar-custom navbar-fixed-top">
<div class="container-fluid">

    <div class="navbar-header page-scroll">

      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#topNavBar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>

      <a id="blog" class="navbar-brand font-smoothing" href="#/blog">Aeris</a>
    </div>

    <div class="collapse navbar-collapse" id="topNavBar">

      <ul class="nav navbar-nav navbar-right">

        <li class="font-smoothing nav-tabs">
          <a id="albums" href="#/albums">ALBUMS</a>
        </li>

        <li class="font-smoothing nav-tabs">
          <a id="artists" href="#/artists">ARTISTS</a>
        </li>

      </ul>
    </div>

</div>
</nav>