class Form {
	constructor(tableDataType, method, enctype) {
		this.parent = document.getElementsByTagName('body')[0];

		var wrap = document.createElement('div'); 
		wrap.className = 'wrap';

		var form = document.createElement('form');
		this.formMethod = method;
		this.formEnctype = enctype;

		var tableResponsiveBootstrap = document.createElement('div'); 
		tableResponsiveBootstrap.className = 'table-responsive';
		tableResponsiveBootstrap.id = tableDataType;
		var createTable = document.createElement('table');
		
		this.parent.appendChild(wrap);
		wrap.appendChild(form);
		form.appendChild(tableResponsiveBootstrap);
		tableResponsiveBootstrap.appendChild(createTable);
	}
}